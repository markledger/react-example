import React, {useState} from 'react';

const MyForm = props => {

  const [person, setPerson] = useState(props.person);

  const processForm = event => {
      event.preventDefault();
      console.log(person);
  }

  const changeHandler = event => {
    const name = event.target.value;
    const updatedPerson = {...person, name};
    setPerson(updatedPerson);
    
  }


  return (
    <form onSubmit={processForm}>
      <input value={person.name} onChange={changeHandler}/>
      <button type='submit'>Save</button>
    </form>
  );

};

export default MyForm;