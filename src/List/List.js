import React, {useState, useEffect} from 'react';
import axios from 'axios';


const List = props => {

  const [data, setData] = useState({results:[], next: ''});
  const [url, setUrl] = useState('https://pokeapi.co/api/v2/pokemon');


  const setUser = (id, name, value) => {


      setData(prevState => {
        
        const newUserList = prevState.results.map(user => {
          if(user.id == id){
            user[name] = value;
          }

          return user
        });
        

        return {
          ...prevState,
          results: newUserList
        }
      });

  }

  useEffect(() => {
  
    const callApi = async () => {
      
        try{

          const response = await axios.get(url);
          
          setData(prevState => (
            {
              ...prevState,
              results: response.data.results,
              next: response.data.next,
            }
          ));

        }catch(err){
          console.error(err);
        }
    };

    callApi();

  }, [url]);


  const clickHandler = event => {
      event.preventDefault();
      setUrl(event.target.href);
  }

 

  return (
    <table className="table table-striped">
    
       { 
          data.results.map((user, index) => {
            return (
              <tr key={index}>
                <td>
                  <userForm user={user} setUser={setUser}/>
                </td>
               </tr>
            );

        })
      }
    
    </table>    
  );

}


const userForm = props => {

  const {user, setUser} = props;

  const saveUser= async () => {
     await axios.post('save/user/'+user.id, user);
  }


  return  (
    <form onSubmit={saveUser}>
      <input name="name" type="text" value={user.name} onChange={(event) => setUser(user.id, 'name', event.target.value)}/>
       <input name="email" type="text" value={user.email} onChange={(event) => setUser(user.id, 'email', event.target.value)}/>
      <button type="submit"/>
    </form>
    );
}

export default List;