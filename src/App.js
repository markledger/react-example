import './App.css';
import List from './List/List';




function App() {

  return (
    <div className="App">
      <header className="App-header">
        <List />
      </header>
    </div>
  );
}

export default App;
